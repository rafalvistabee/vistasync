import logging
import pprint
import re
import os
import sys
import calendar
from collections import namedtuple
from urllib.parse import urlparse
from datetime import datetime
import shutil
from vbclient import VistabeeClient
from PyQt5.QtCore import QSettings, QStandardPaths, QDateTime
from PyQt5.QtSql import QSqlDatabase, QSqlQuery
import dateutil.parser

DbFile = namedtuple('DbFile', 'path lastModified remoteId stMTime stSize')


def splitPath(path):
    """Split path into tuples"""
    norm = os.path.normpath(path)
    return norm.split(os.path.sep)


def getSyncTree(data):
    root = DirNode()
    for folder in data['empty_folders']:
        parts = folder.split('/')
        root.addPath(parts)

    for f in data['files']:
        parts = f['folder'].split('/')
        root.addPath(parts)

    return root

def parseRemoteDateTime(dtString):
    dt = dateutil.parser.parse(dtString)
    return datetime.fromtimestamp(calendar.timegm(dt.timetuple()))  # pylint: disable=no-member


class AbortSyncError(Exception):
    pass


class PushItem(object):  # pylint: disable=too-few-public-methods

    def __init__(self, syncDir, relPath):
        self.syncDir = syncDir
        self.relPath = relPath
        self.fullPath = os.path.join(syncDir, relPath)
        stat_result = os.stat(self.fullPath)
        self.stSize = stat_result.st_size
        self.stMTime = stat_result.st_mtime

    def __str__(self):
        return '{} ({}, {})'.format(self.relPath, self.stSize, self.stMTime)

    def __repr__(self):
        return self.__str__()


class DirNode(object):

    def __init__(self, value=None, parent=None):
        self.children = dict()
        self.value = value
        self.parent = parent

    def __str__(self):
        path = []
        parent = self
        while parent is not None and parent.value is not None:
            path.append(parent.value)
            parent = parent.parent

        return '/'.join(reversed(path))

    def addPath(self, parts):
        root = self
        for part in parts:
            if part not in root.children:
                root.children[part] = DirNode(part, root)
            root = root.children[part]

    def inTree(self, parts):
        """Is path in tree, path must be past as tuple of directories (parts)"""
        if len(parts) < 1:
            return True

        try:
            root = self.children[parts[0]]
        except KeyError:
            return False

        return root.inTree(parts[1:])

    def walk(self):
        yield self
        for child in self.children.values():
            yield from child.walk()

    def printFull(self):
        for node in self.walk():
            if not node.children:
                logging.debug(node)


class FilesDb(object):
    """Local database of files that keeeps remote id and last modification time"""

    def __init__(self):
        dataDir = QStandardPaths.writableLocation(QStandardPaths.AppLocalDataLocation)
        os.makedirs(dataDir, exist_ok=True)
        dbPath = os.path.join(dataDir, 'database.db')
        db = QSqlDatabase.addDatabase('QSQLITE')
        db.setDatabaseName(dbPath)
        db.open()

        self._runQuery("""
            CREATE TABLE IF NOT EXISTS files (
                path VARCHAR(2048) UNIQUE PRIMARY KEY,
                remoteId INTEGER UNIQUE,
                lastModified DATETIME,
                stMTime FLOAT,
                stSize INTEGER
            )
        """)

        self._runQuery("""
            CREATE TABLE IF NOT EXISTS fetching (
                path VARCHAR(2048) UNIQUE PRIMARY KEY
            )
        """)

    def _runQuery(self, sql, data=None):
        if data is None:
            data = []
        query = QSqlQuery()
        query.prepare(sql)
        for k, v in data:
            query.bindValue(k, v)

        if not query.exec_():
            logging.error('SQL error: %s; %s\n%s', sql, data, query.lastError().text())
        return query

    def getFetchingFiles(self):
        query = self._runQuery("SELECT path FROM fetching")
        while query.next():
            yield query.value(0)

    def storeFetchingFile(self, path):
        sql = "INSERT OR REPLACE INTO fetching (path) VALUES (:path)"
        data = [
            (":path", path),
        ]
        self._runQuery(sql, data)

    def deleteFetchingFile(self, path):
        sql = "DELETE FROM fetching WHERE path = :path"
        data = [
            (":path", path),
        ]
        self._runQuery(sql, data)

    def getFileList(self):
        sql = "SELECT path, lastModified, remoteId, stMTime, stSize FROM files"
        query = self._runQuery(sql)

        while query.next():
            path = query.value(0)
            lastModified = datetime.strptime(query.value(1), '%Y-%m-%d %H:%M:%S')
            remoteId = int(query.value(2))
            stMTime = float(query.value(3))
            stSize = int(query.value(4))
            yield DbFile(path, lastModified, remoteId, stMTime, stSize)

    def storeFile(self, path, lastModified, remoteId, stMTime, stSize):
        sql = ("""
            INSERT OR REPLACE INTO files (path, remoteId, lastModified, stMTime, stSize)
            VALUES (:path, :remoteId, :lastModified, :stMTime, :stSize)
        """)
        data = [
            (":path", path),
            (":remoteId", remoteId),
            (":lastModified", lastModified.strftime('%Y-%m-%d %H:%M:%S')),
            (":stSize", stSize),
            (":stMTime", stMTime),
        ]
        self._runQuery(sql, data)
        return DbFile(path, lastModified, remoteId, stMTime, stSize)

    def deleteFile(self, path):
        sql = "DELETE FROM files WHERE path = :path"
        data = [(":path", path),]
        self._runQuery(sql, data)


class Synchronizer(object):
    """The class responsible for synchronizing files"""

    filenameSubRe = re.compile(r'[^a-zA-Z0-9\-_\.]')

    def __init__(self, parent):
        self.parent = parent
        self.vbclient = VistabeeClient(parent, self)
        self.db = FilesDb()
        self.fetchList = []
        self.pushList = []
        self.deleteLocalList = []
        self.deleteRemoteList = []
        self.conflictList = []
        self.lock = False
        self.activeDownloads = [] #  this exists to keep dectructor from removing http reply too soon
        self.activeUploads = [] #  this exists to keep dectructor from removing http reply too soon

    def syncNow(self, silent=False):
        if self.lock:
            if not silent:
                self.parent.showError('Syncing not started', 'Syncing is already in progress')
            return
        try:
            self.getSyncDir()
        except AbortSyncError:
            return
        self.lock = True
        authReply = self.vbclient.authenticate()
        authReply.finished.connect(self.handleAuthenticated)

        current = QDateTime.currentDateTime()
        settings = QSettings('VistaBee', 'VistaSync')
        settings.setValue('lastSync', current.toString('h:mm ap, MMMM d'))

    def syncDirRelative(self, absolutePath):
        return os.path.relpath(absolutePath, self.getSyncDir())

    def finishSync(self):
        self.lock = False
        self.parent.ui.statusLabel.setText('Synced')
        logging.info('Sync complete')

    def getSyncDir(self):
        settings = QSettings('VistaBee', 'VistaSync')
        syncDir = settings.value('syncDir')
        if not os.path.exists(syncDir):
            try:
                os.makedirs(syncDir, exist_ok=True)
            except OSError:
                logging.error('Sync Dir does not exist and its creation has failed: %s', syncDir)
                raise AbortSyncError()

        if sys.platform == 'win32':
            from winfolder import seticon
            from vistasync import projectPath
            seticon(syncDir, projectPath('icons', 'folder.ico'), 0)
        return syncDir

    def handleAuthenticated(self):
        self.vbclient.get('files-to-sync/', self.handleFilesToSync)

    def handleFilesToSync(self, reply):
        data = reply.getJsonData()
        self.clearLocalData(data)
        self.getSyncPlan(data['files'])
        self.printPlan()

        self.fixConflicts()
        self.fetch()

    def fixConflicts(self):
        if not self.conflictList:
            return
        syncDir = self.getSyncDir()
        conflictDir = os.path.join(syncDir, 'CONFLICT')
        message = '{} files in conflict, moving to {}'.format(len(self.conflictList), conflictDir)
        self.parent.showError('Files conflict', message)
        os.makedirs(conflictDir, exist_ok=True)

        for path in self.conflictList:
            parts = splitPath(path)
            target = os.path.join(conflictDir, *parts[:-1])
            os.makedirs(target, exist_ok=True)

            target = os.path.join(target, parts[-1])
            source = os.path.join(syncDir, path)

            shutil.move(source, target)

    def _fetchFinished(self, localPath, remote):
        lastModified = parseRemoteDateTime(remote['last_modified'])
        path = self.syncDirRelative(localPath)
        stat_result = os.stat(localPath)
        self.db.storeFile(path, lastModified, remote['id'],
                          stat_result.st_mtime, stat_result.st_size)
        self.db.deleteFetchingFile(localPath)

        if self.fetchList:
            localPath, remote = self.fetchList.pop()
            self._makeFetch(localPath, remote)
        else:
            self.activeDownloads = []
            self.push()

    def _makeFetch(self, localPath, remote):
        logging.debug('Fetching %s', localPath)
        self.db.storeFetchingFile(localPath)
        reply = self.vbclient.downloadFile(remote['file'], localPath)
        self.activeDownloads.append(reply)
        reply.finished.connect(lambda: self._fetchFinished(localPath, remote))

    def fetch(self):
        if not self.fetchList:
            self.push()

        for _ in range(2):
            try:
                localPath, remote = self.fetchList.pop()
            except IndexError:
                return
            self._makeFetch(localPath, remote)

    def _pushFinished(self, pushItem, remote):
        try:
            lastModified = parseRemoteDateTime(remote['last_modified'])
        except KeyError:
            logging.error('Missing last_modified\n\t%s', remote)
            return

        try:
            remote_id = remote['id']
        except KeyError:
            logging.error('Missing remote id\n\t%s', remote)
            return

        self.db.storeFile(pushItem.relPath, lastModified, remote_id,
                          pushItem.stMTime, pushItem.stSize)
        if self.pushList:
            pushItem = self.pushList.pop()
            self._makePush(pushItem)
        else:
            self.activeUploads = []
            self.deleteLocal()

    def _makePush(self, pushItem):
        reply = self.vbclient.uploadFile(pushItem.syncDir, pushItem.relPath)
        self.activeUploads.append(reply)
        reply.finished.connect(lambda data: self._pushFinished(pushItem, data))

    def push(self):
        if not self.pushList:
            self.deleteLocal()

        for _ in range(2):
            try:
                pushItem = self.pushList.pop()
            except IndexError:
                return
            self._makePush(pushItem)

    def deleteLocal(self):
        syncDir = self.getSyncDir()
        for path in self.deleteLocalList:
            try:
                os.remove(os.path.join(syncDir, path))
                self.db.deleteFile(path)
            except (FileExistsError, FileNotFoundError):
                logging.error('Cannot delete %s', path)

        self.deleteRemote()

    def deleteFinished(self, reply, relPath):
        if reply.getStatusCode() == 204:
            self.db.deleteFile(relPath)
        elif reply.getStatusCode() == 400:
            logging.error('No delete permission. Dropping dropping from local db.')
            self.db.deleteFile(relPath)
        else:
            logging.error('Delete for %s returned %s', relPath, reply.getStatusCode())

    def deleteRemote(self):
        for dbFile in self.deleteRemoteList:
            url = 'assetfile/{}/'.format(dbFile.remoteId)
            callback = lambda reply, path=dbFile.path: self.deleteFinished(reply, path)  # pylint: disable=cell-var-from-loop
            self.vbclient.delete(url, callback)
        self.finishSync()

    def clearLocalData(self, data):
        tree = getSyncTree(data)
        syncDir = self.getSyncDir()
        for dirpath, dirnames, _ in os.walk(syncDir):
            for dirname in dirnames:
                fullPath = os.path.join(dirpath, dirname)
                relPath = os.path.relpath(fullPath, syncDir)
                if relPath.startswith('CONFLICT'):
                    continue
                parts = splitPath(relPath)
                if not tree.inTree(parts):
                    logging.debug('removing "%s"', fullPath)
                    shutil.rmtree(fullPath)

        for dbFile in self.db.getFileList():
            dbDir = os.path.dirname(dbFile.path)
            parts = splitPath(dbDir)
            fullPath = os.path.join(syncDir, parts[0])
            if not tree.inTree(parts):
                logging.debug('Drop %s from db', dbFile.path)
                self.db.deleteFile(dbFile.path)
            elif not os.path.exists(fullPath):
                logging.debug('Folder for %s has dissapeared. Dropping from local'
                              ' database', parts[0])
                self.db.deleteFile(dbFile.path)

        for path in self.db.getFetchingFiles():
            logging.debug('Dropping incomplete download %s', path)
            try:
                os.remove(path)
            except FileNotFoundError:
                logging.error('Cannot drop %s', path)
            self.db.deleteFetchingFile(path)

    def _remoteFilesAsDict(self, files):
        remoteFiles = dict()
        for f in files:
            parts = urlparse(f['file'])
            basename = os.path.basename(parts.path)
            localParts = f['folder'].split('/')
            localParts.append(basename)
            localPath = os.path.join(*localParts)
            remoteFiles[localPath] = f
        return remoteFiles

    def clearName(self, fullPath):
        """Make sure the file name is acceptable"""
        lead, baseName = os.path.split(fullPath)
        clearName = self.filenameSubRe.sub('_', baseName)
        if baseName != clearName:
            message = ('Illegal characters in filename. Renaming "{}" to "{}"'
                       .format(baseName, clearName))
            self.parent.showError('Renaming files', message)
            logging.info('Renamig files: %s', message)
            os.rename(fullPath, os.path.join(lead, clearName))
        return fullPath

    def getSyncPlan(self, files):  # pylint: disable=too-many-locals,too-many-branches
        remoteFiles = self._remoteFilesAsDict(files)
        dbFiles = {f.path: f for f in self.db.getFileList()}

        localPaths = set()
        syncDir = self.getSyncDir()

        for dirpath, _, filenames in os.walk(syncDir):
            for filename in filenames:
                fullPath = self.clearName(os.path.join(dirpath, filename))
                relPath = os.path.relpath(fullPath, syncDir)
                if (filename.startswith('.')
                        or filename.startswith('__')
                        or filename == 'desktop.ini'
                        or relPath.startswith('CONFLICT')):
                    continue

                localPaths.add(relPath)
                try:
                    remote = remoteFiles.pop(relPath)
                except KeyError:
                    remote = None

                try:
                    dbFile = dbFiles.pop(relPath)
                except KeyError:
                    dbFile = None

                if remote is None:
                    if dbFile is None:
                        self.pushList.append(PushItem(syncDir, relPath))
                    else:
                        self.deleteLocalList.append(relPath)
                else:
                    if dbFile is None:
                        self.conflictList.append(relPath)
                        source = os.path.join(syncDir, relPath)
                        self.fetchList.append([source, remote])
                    else:
                        # file exists everywhere
                        remoteLastModified = parseRemoteDateTime(remote['last_modified'])
                        stat_result = os.stat(fullPath)
                        mustPush = (dbFile.lastModified > remoteLastModified or
                                    stat_result.st_size != dbFile.stSize or
                                    stat_result.st_mtime != dbFile.stMTime)
                        if mustPush:
                            self.pushList.append(PushItem(syncDir, relPath))
                        elif dbFile.lastModified < remoteLastModified:
                            source = os.path.join(syncDir, relPath)
                            self.fetchList.append([source, remote])

        for relPath, remote in remoteFiles.items():
            if relPath not in dbFiles.keys():
                source = os.path.join(syncDir, relPath)
                self.fetchList.append([source, remote])

        self.deleteRemoteList = [v for k, v in dbFiles.items() if k not in localPaths]

    def printPlan(self):
        pp = pprint.PrettyPrinter(indent=4)
        logging.debug('Changes detected. Fetching %s, uploading %s, deleting localy %s,'
                      ' deleting remotely %s, conflict %s',
                      len(self.fetchList), len(self.pushList),
                      len(self.deleteLocalList), len(self.deleteRemoteList),
                      len(self.conflictList))
        logging.debug('Fetch: %s', pp.pformat(self.fetchList))
        logging.debug('Push: %s', pp.pformat(self.pushList))
        logging.debug('Delete local: %s', pp.pformat(self.deleteLocalList))
        logging.debug('Delete remote: %s', pp.pformat(self.deleteRemoteList))
        logging.debug('Conflict: %s', pp.pformat(self.conflictList))
