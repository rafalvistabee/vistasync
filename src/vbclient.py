import os
import json
import logging
from urllib.parse import urljoin
from PyQt5.QtCore import QObject, QUrl, pyqtSignal, QSettings, QFile
from PyQt5.QtNetwork import QNetworkAccessManager, QNetworkRequest


BASEURL = os.environ.get('BASEURL', 'http://www.vistabee.com/v2/api/')


def encode(value):
    return bytes(value.encode('utf-8'))


class AuthReply(QObject):

    finished = pyqtSignal()


class AsyncDownload(QObject):

    finished = pyqtSignal()

    def __init__(self, manager, source, target):
        self.manager = manager
        self.source = source
        request = QNetworkRequest()
        request.setUrl(QUrl(source))

        self._mkdir(target)
        self.target = open(target, 'wb')

        self.reply = self.manager.get(request)
        self.reply.readyRead.connect(self._readyRead)
        self.reply.finished.connect(self._finished)
        super().__init__()

    def _readyRead(self):
        self.target.write(bytes(self.reply.readAll()))

    def _finished(self):
        self.target.write(self.reply.readAll())
        self.target.close()
        self.finished.emit()
        self.reply.deleteLater()

    def _mkdir(self, path):
        lead, _ = os.path.split(path)
        os.makedirs(lead, exist_ok=True)


class AsyncUpload(QObject):

    finished = pyqtSignal(dict)
    sourceQFile = None

    def __init__(self, vbclient, syncDir, relPath):
        self.manager = vbclient.manager
        self.vbclient = vbclient
        self.relPath = relPath
        self.source = os.path.join(syncDir, relPath)

        logging.debug(relPath)
        vbclient.post('signfile/', {'filename': relPath}, self._performUpload)
        super().__init__()

    def _performUpload(self, reply):
        data = reply.getJsonData()
        reply.deleteLater()

        self.sourceQFile = QFile(self.source)
        self.sourceQFile.open(QFile.ReadOnly)
        request = QNetworkRequest()
        try:
            signedUrl = data['signed_url']
        except KeyError:
            logging.error('No signed url. Upload failed.\n\t%s', data)
            return
        request.setUrl(QUrl(signedUrl))

        reply = self.manager.put(request, self.sourceQFile)
        reply.finished.connect(lambda: self._pushToVistabee(reply, signedUrl))

    def _pushToVistabee(self, reply, signedUrl):
        self.sourceQFile.close()
        reply.deleteLater()
        self.vbclient.post('assetfile/', {'file': signedUrl}, self._finished)

    def _finished(self, reply):
        data = reply.getJsonData()
        self.finished.emit(data)
        reply.deleteLater()




class VistabeeClient(object):
    version = 1
    _token = None

    def __init__(self, parent, synchronizer):
        self.parent = parent  # qt parent
        logging.info('Base url: %s', BASEURL)
        self.manager = QNetworkAccessManager()
        self.manager.networkAccessibleChanged.connect(self.networkAccessibleChanged)
        self.synchronizer = synchronizer

    def getStatusCode(self, reply):  # pylint: disable=no-self-use
        return reply.attribute(QNetworkRequest.HttpStatusCodeAttribute)

    def getJsonData(self, reply):
        content = bytes(reply.readAll()).decode('utf-8')
        try:
            return json.loads(content)
        except ValueError:
            self.errorHandler(reply)

    def setHeaders(self, request, contentType):
        request.setHeader(QNetworkRequest.UserAgentHeader, 'VistaSync v{}'.format(self.version))
        request.setHeader(QNetworkRequest.ContentTypeHeader, contentType)
        if self._token is not None:
            request.setRawHeader(encode('Authorization'),
                                 encode('Bearer ' + self._token))

    def makeUrl(self, relative):
        return QUrl(urljoin(BASEURL, relative))

    def authenticate(self):
        settings = QSettings('VistaBee', 'VistaSync')
        data = {
            'username': settings.value('username'),
            'password': settings.value('password'),
        }
        authReply = AuthReply()
        self.post('login/', data, lambda reply: self.finishedAuthenticate(reply, authReply))
        return authReply

    def finishedAuthenticate(self, reply, authReply):
        statusCode = self.getStatusCode(reply)
        if statusCode is None or statusCode >= 300:
            self.errorHandler(reply)
            return

        data = self.getJsonData(reply)
        self._token = data['access_token']
        logging.debug('Received Token: %s', self._token)
        authReply.finished.emit()

    def makeRequest(self, method, path, callback, contentType, data=None):
        url = self.makeUrl(path)
        request = QNetworkRequest()
        request.setUrl(url)
        self.setHeaders(request, contentType)

        method = getattr(self.manager, method)
        if data is None:
            reply = method(request)
        else:
            reply = method(request, data)

        requestArgs = [method, path, callback, contentType, data]
        reply.getJsonData = lambda: self.getJsonData(reply)
        reply.getStatusCode = lambda: self.getStatusCode(reply)
        reply.finished.connect(lambda: callback(reply))
        return reply

    def errorHandler(self, reply):
        statusCode = self.getStatusCode(reply)
        logging.error('Status code: %s', statusCode)

        if statusCode is None:
            message = 'Cannot connect to VistaBee'
        else:
            content = bytes(reply.readAll()).decode('utf-8')
            logging.error(content)

            try:
                response = json.loads(content)
            except ValueError:
                message = 'Unknown error'
            else:
                if 'non_field_errors' in response:
                    message = '\n'.join(response.pop('non_field_errors'))
                else:
                    message = ''

                for k, v in response.items():
                    message += '{}: {}\n'.format(k, v)

        self.parent.showError('Connection error', message)
        self.synchronizer.lock = False

    def networkAccessibleChanged(self, accessible):
        if accessible == QNetworkAccessManager.NotAccessible:
            verboseAccessible = 'Not Accessible'
            self.synchronizer.lock = False
            self.parent.showError('Network is not accessible', 'Syncing aborted.')
            self.manager = QNetworkAccessManager()
            logging.info('Network is not accessible')

    # high level api starts here
    def get(self, path, callback, contentType='application/json'):
        return self.makeRequest('get', path, callback, contentType)

    def post(self, path, data, callback, contentType='application/json'):
        bytes_data = encode(json.dumps(data))
        return self.makeRequest('post', path, callback, contentType, data=bytes_data)

    def delete(self, path, callback, contentType='application/json'):
        return self.makeRequest('deleteResource', path, callback, contentType)

    def downloadFile(self, source, target):
        return AsyncDownload(self.manager, source, target)

    def uploadFile(self, syncDir, relPath):
        """Sign file with vistabee api and upload directly to s3

        We delegate remote path construction to the api. just path relative path
        from vistasync for signing and do raw put on s3.

        syncDir -- app syncDir
        relPath -- path relative to syncDir, should give valid abs path after joining with syncDir

        """
        return AsyncUpload(self, syncDir, relPath)
