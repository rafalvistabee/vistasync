#!/usr/bin/env python
import os
import sys
import signal
import logging
from PyQt5.QtWidgets import (QApplication, QSystemTrayIcon, QTabWidget,
                             QMessageBox, QMenu, QLineEdit, QFileDialog)
from PyQt5.QtCore import QTimer, QSize, QSettings, QStandardPaths
from PyQt5.QtGui import QIcon
from ui.mainwindow import Ui_TabWidget  # pylint: disable=import-error
from synchronizer import Synchronizer


class Window(QTabWidget):

    _reallyClose = False

    def __init__(self):
        super().__init__()
        self.settings = QSettings('VistaBee', 'VistaSync')
        self.synchronizer = Synchronizer(self)
        # Set up the user interface from Designer.
        self.ui = Ui_TabWidget()
        self.ui.setupUi(self)
        self.ui.passwordInput.setEchoMode(QLineEdit.Password)

        self.initSettings()

        # connect signals
        self.ui.okBtn1.clicked.connect(self.saveAndClose)
        self.ui.okBtn2.clicked.connect(self.saveAndClose)
        self.ui.browseBtn.clicked.connect(self.selectWorkdir)

        self.createTrayIcon()
        self.syncTimer = QTimer()
        self.syncTimer.timeout.connect(self.periodicalSync)
        self.updateTimer()

    def closeEvent(self, event):
        if not self._reallyClose:
            self.hide()
            event.ignore()

    def reallyClose(self):
        self._reallyClose = True
        self.close()

    def periodicalSync(self):
        if self.ui.syncToggle.isChecked():
            self.syncNow(silent=True)

    def updateTimer(self):
        intervalMinutes = self.settings.value('interval', 10, type=int)
        if intervalMinutes > 0:
            self.syncTimer.setInterval(intervalMinutes * 1000 * 60)
            self.syncTimer.start()
        else:
            self.syncTimer.stop()

    def selectWorkdir(self):
        dialog = QFileDialog(self)
        dialog.setFileMode(QFileDialog.Directory)
        dialog.setOption(QFileDialog.ShowDirsOnly, True)

        if dialog.exec_():
            for directory in dialog.selectedFiles():
                self.settings.setValue('syncDir', directory)
                self.ui.syncDirInput.setText(directory)

    def toggleWindow(self, reason):
        if reason != 1:
            if self.isVisible():
                self.hide()
            else:
                self.show()

    def showSettings(self):
        self.show()
        self.setCurrentIndex(1)

    def createTrayIcon(self):
        # setup tray icon
        self.trayIcon = QSystemTrayIcon(self.windowIcon(), self)
        self.trayIcon.activated.connect(self.toggleWindow)

        menu = QMenu(self)

        settingsAction = menu.addAction("Settings")
        settingsAction.triggered.connect(self.showSettings)

        exitAction = menu.addAction("Exit")
        exitAction.triggered.connect(self.reallyClose)

        syncAction = menu.addAction("Sync Now")
        syncAction.triggered.connect(self.syncNow)

        self.trayIcon.setContextMenu(menu)
        self.trayIcon.show()

    def initSettings(self):
        self.ui.usernameInput.setText(self.settings.value('username'))
        self.ui.passwordInput.setText(self.settings.value('password'))
        self.ui.syncDirInput.setText(self.settings.value('syncDir'))
        self.ui.syncToggle.setChecked(self.settings.value('syncToggle', True, type=bool))
        self.ui.intervalInput.setValue(self.settings.value('interval', 5, type=int))
        self.ui.lastSyncTime.setText('Last synced: ' + self.settings.value('lastSync', 'unknown'))

        # window settings
        size = self.settings.value("size")
        pos = self.settings.value("pos")
        if size and pos:
            self.resize(size)
            self.move(pos)

    def saveSettings(self):
        self.settings.setValue('username', self.ui.usernameInput.text())
        self.settings.setValue('password', self.ui.passwordInput.text())
        self.settings.setValue('syncDir', self.ui.syncDirInput.text())
        self.settings.setValue('interval', self.ui.intervalInput.value())
        self.settings.setValue('syncToggle', self.ui.syncToggle.isChecked())

        isSetup = (self.ui.usernameInput.text() and
                   self.ui.passwordInput.text() and
                   self.ui.syncDirInput.text())
        self.settings.setValue('isSetup', isSetup)

        self.settings.setValue("size", self.size())
        self.settings.setValue("pos", self.pos())
        self.updateTimer()

    def syncNow(self, silent=False):
        self.synchronizer.syncNow(silent=silent)
        self.ui.statusLabel.setText('Synchronizing')

    def saveAndClose(self):
        self.saveSettings()
        self.hide()

    def showError(self, title, message):
        self.trayIcon.showMessage(title, message, QSystemTrayIcon.Warning)

    def showInfo(self, title, message):
        self.trayIcon.showMessage(title, message, QSystemTrayIcon.Information)


def sigintHandler(signum, frame):  # pylint: disable=unused-argument
    logging.info('\rSIGINT caught. Exiting...')
    sys.exit()


def projectPath(*relative):
    currentDir = os.path.dirname(sys.argv[0])
    if currentDir == 'src':
        # fallback that sometimes works, usefull only for debugging
        # (when you run python interpreter directly)
        currentDir = os.path.join(os.getcwd(), 'src')
    return os.path.join(currentDir, *relative)


def run():
    logDir = QStandardPaths.writableLocation(QStandardPaths.AppLocalDataLocation)
    logPath = os.path.join(logDir, 'vistasync.log')
    try:
        os.remove(logPath)
    except OSError:
        pass
    print(logPath)
    logging.basicConfig(filename=logPath, level=logging.DEBUG)

    signal.signal(signal.SIGINT, sigintHandler)

    app = QApplication(sys.argv)

    if not QSystemTrayIcon.isSystemTrayAvailable():
        QMessageBox.critical(None, "Systray",
                             "Cannot detect any system tray on this system.")
        sys.exit(1)

    QApplication.setQuitOnLastWindowClosed(True)

    # setup icon
    appIcon = QIcon()
    appIcon.addFile(projectPath('icons', '16x16.png'), QSize(16, 16))
    appIcon.addFile(projectPath('icons', '32x32.png'), QSize(32, 32))
    appIcon.addFile(projectPath('icons', '48x48.png'), QSize(48, 48))
    appIcon.addFile(projectPath('icons', '256x256.png'), QSize(256, 256))
    appIcon.addFile(projectPath('256x256.icns'), QSize(256, 256))
    app.setWindowIcon(appIcon)

    # construct main app window
    window = Window()
    settings = QSettings('VistaBee', 'VistaSync')
    if settings.value('isSetup', False, type=bool):
        window.periodicalSync()
    else:
        window.show()

    # hack around python loop not being able to catch sigint
    eventTimer = QTimer()
    eventTimer.start(500)
    eventTimer.timeout.connect(lambda: None)

    sys.exit(app.exec_())


if __name__ == '__main__':
    run()
