# -*- mode: python -*-

block_cipher = None


a = Analysis(['src/vistasync.py'],
             pathex=['/Users/Editor/vistasync'],
             binaries=None,
             datas=[('src/icons/', 'icons/'),],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='vistasync',
          debug=False,
          strip=False,
          upx=True,
          icon='src/icons/256x256.icns',
          console=False )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='vistasync')
app = BUNDLE(coll,
             name='vistasync.app',
             icon='src/icons/256x256.icns',
             bundle_identifier=None)
